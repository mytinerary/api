const mongoose = require('mongoose')

const itinerarySchema = mongoose.Schema({
  title: {
    type: String,
    required: [true, 'The field title is required'],
    trim: true
  },
  img: {
    type: String,
    required: [true, 'The field img is required'],
    trim: true
  },
  activities: [
    {
      name: { type: String, trim: true },
      img: { type: String, trim: true }
    }
  ],
  authorName: {
    type: String,
    trim: true,
    required: [true, 'The field authorName is required']
  },
  authorPic: {
    type: String,
    trim: true,
    required: [true, 'The field authorPic is required']
  },
  price: {
    type: Number,
    required: [true, 'The field price is required'],
    min: [1, 'The price must be equal to or greater than 1'],
    max: [5, 'The price must be equal to or lower than 5']
  },
  duration: {
    type: Number,
    required: [true, 'The field duration is required'],
    min: [1, 'The duration must be equal to or greater than 1']
  },
  likes: {
    type: Number,
    default: 0
  },
  hashtags: [{ type: String, trim: true }],
  comments: [
    {
      userId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'user'
      },
      text: { type: String, trim: true },
      userName: { type: String, trim: true },
      userPic: { type: String, trim: true }
    }
  ],
  userLike: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'user'
  }],
  cityId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'city'
  }

})

const Itinerary = mongoose.model('itinerary', itinerarySchema)

module.exports = Itinerary
