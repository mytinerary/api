const mongoose = require('mongoose')

const citySchema = mongoose.Schema({
  name: {
    type: String,
    required: [true, 'The field name is required'],
    trim: true
  },
  country: {
    type: String,
    required: [true, 'The field country is required'],
    trim: true
  },
  img: {
    type: String,
    required: [true, 'The field img is required'],
    trim: true
  }
})

const City = mongoose.model('city', citySchema)

module.exports = City
