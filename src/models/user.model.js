const mongoose = require('mongoose')

const userSchema = mongoose.Schema({
  firstName: {
    type: String,
    required: [true, 'The field firstName is required'],
    trim: true
  },
  lastName: {
    type: String,
    required: [true, 'The field lastName is required'],
    trim: true
  },
  email: {
    type: String,
    required: [true, 'The field email is required'],
    trim: true
  },
  password: {
    type: String,
    required: [true, 'The field password is required'],
    trim: true
  },
  userPic: {
    type: String,
    required: [true, 'The field userPic is required'],
    trim: true
  },
  country: {
    type: String,
    required: [true, 'The field country is required'],
    trim: true
  },
  favoriteItineraries: [
    {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'itinerary'
    }
  ]
})

const User = mongoose.model('user', userSchema)

module.exports = User
