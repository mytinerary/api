const express = require('express')
const cors = require('cors')
const passport = require('passport')
const { connectDB, closeConnection } = require('../db/mongodb')

class Server {
  constructor () {
    this.app = express()
    this.port = process.env.PORT || 5000

    this.connectDB()

    this.middlewares()

    this.routes()
  }

  async connectDB () {
    this.connection = await connectDB()
  }

  disconnectDB () {
    closeConnection()
  }

  middlewares () {
    this.app.use(express.json())

    this.app.use(express.urlencoded({
      extended: true
    })
    )

    this.app.use(cors())

    this.app.use(passport.initialize())

    require('../../passport')
  }

  routes () {
    this.app.use('/api/cities', require('../routes/city.routes'))
    this.app.use('/api/itineraries', require('../routes/itinerary.routes'))
    this.app.use('/api/user', require('../routes/user.routes'))
    this.app.use('/api/like', require('../routes/like.routes'))
    this.app.use('/api/comments', require('../routes/comment.routes'))
    this.app.use('/api/checkuser', require('../routes/checkuser.routes'))
  }

  listen () {
    return this.app.listen(this.port, () => {
      console.log('El servidor se está ejecutando en el port ' + this.port)
    })
  }
}

module.exports = Server
