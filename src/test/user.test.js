require('dotenv').config()
const Server = require('../models/server')
const supertest = require('supertest')
const User = require('../models/user.model')
const bcrypt = require('bcrypt')
const saltRounds = 10
const server = new Server()
let testServer
let api

const initialUsers = [
  { firstName: 'user1', lastName: 'user1', email: 'user1@test.com', password: bcrypt.hashSync('1234', saltRounds), userPic: 'img.jpg', country: 'country' },
  { firstName: 'user2', lastName: 'user2', email: 'user2@test.com', password: bcrypt.hashSync('1234', saltRounds), userPic: 'img.jpg', country: 'country' }
]
let token
beforeAll(async () => {
  testServer = server.listen()
  api = supertest(testServer)
})
beforeEach(async () => {
  const user1 = new User(initialUsers[0])
  await user1.save()
  const user2 = new User(initialUsers[1])
  await user2.save()
  const response = await api.post('/api/user/signin')
    .send({
      email: initialUsers[0].email,
      password: '1234'
    })
  token = response.body.response.token
})

afterEach(async () => {
  await User.deleteMany({})
})

afterAll(async () => {
  await User.deleteMany({})

  await server.disconnectDB()
  await testServer.close()
})

describe('GET /user', () => {
  const endpoint = '/api/user'
  test('It responds with status 401 Unauthorized', async () => {
    const response = await api.get(endpoint)
    expect(response.statusCode).toBe(401)
  })
  test('It responds with status 200 OK', async () => {
    const response = await api.get(endpoint).set('Authorization', `Bearer ${token}`)
    expect(response.statusCode).toBe(200)
    expect(response.body.success).toBe(true)
  })
  test('It responds with an array of all users in database', async () => {
    const response = await api.get(endpoint).set('Authorization', `Bearer ${token}`)
    expect(response.body.total).toBe(2)
    expect(response.body).not.toBeNull()
    expect(Array.isArray(response.body.response)).toBe(true)
  })

  test('It responds with the correct data of users', async () => {
    const response = await api.get(endpoint).set('Authorization', `Bearer ${token}`)

    expect(response.body.response[0]).toMatchObject(initialUsers[0])
    expect(response.body.response[1]).toMatchObject(initialUsers[1])
  })
})

// POST
describe('POST /user/signup', () => {
  const endpoint = '/api/user/signup'

  test('It responds with status 201', async () => {
    const newUser = { firstName: 'user', lastName: 'user', email: 'user@test.com', password: '1234', userPic: 'img.jpg', country: 'country' }
    const response = await api.post(endpoint)
      .send(newUser)
    expect(response.statusCode).toBe(201)
  })

  test('It responds with the newly created user', async () => {
    const newUser = { firstName: 'user', lastName: 'user', email: 'user@test.com', password: '1234', userPic: 'img.jpg', country: 'country' }
    const response = await api.post(endpoint)
      .send(newUser)
    expect(response.statusCode).toBe(201)
    expect(response.body.response.response.firstName).toBe(newUser.firstName)
  })

  test('make sure a user was added to the database', async () => {
    const newUser = { firstName: 'user', lastName: 'user', email: 'user@test.com', password: '1234', userPic: 'img.jpg', country: 'country' }
    await api.post(endpoint)
      .send(newUser)
    const response = await api.get('/api/user').set('Authorization', `Bearer ${token}`)
    expect(response.body.total).toBe(initialUsers.length + 1)
  })

  test('make sure a wrong object is not added', async () => {
    const newUser = { wrong: 'user', lastName: 'user', email: 'user@test.com', password: '1234', userPic: 'img.jpg', country: 'country' }
    await api.post(endpoint)
      .send(newUser)
    const response = await api.get('/api/user').set('Authorization', `Bearer ${token}`)
    expect(response.body.total).toBe(initialUsers.length)
  })
})

describe('POST /users/signin', () => {
  const endpoint = '/api/user/signin'

  test('It responds with status 200', async () => {
    const user = {
      email: initialUsers[0].email,
      password: '1234'
    }
    const response = await api.post(endpoint)
      .send(user)
    expect(response.statusCode).toBe(200)
  })

  test('It responds with status 500', async () => {
    const user = {
      email: 'notExists@test.com',
      password: '1234'
    }
    const response = await api.post(endpoint)
      .send(user)
    expect(response.statusCode).toBe(500)
  })
})
