require('dotenv').config()
const Server = require('../models/server')
const supertest = require('supertest')
const City = require('../models/city.model')

const server = new Server()
let testServer
let api

const initialCities = [
  { name: 'Buenos Aires', img: 'buenosAires.jpg', country: 'Argentina' },
  { name: 'Montevideo', img: 'montevideo.jpg', country: 'Uruguay' }
]
beforeAll(async () => {
  testServer = server.listen()
  api = supertest(testServer)
})
beforeEach(async () => {
  await City.deleteMany({})

  const city1 = new City(initialCities[0])
  await city1.save()
  const city2 = new City(initialCities[1])
  await city2.save()
})

afterEach(async () => {
  await City.deleteMany({})
})

afterAll(async () => {
  await City.deleteMany({})

  await server.disconnectDB()
  testServer.close()
})

describe('GET /cities', () => {
  const endpoint = '/api/cities'
  test('It responds with status 200 OK', async () => {
    const response = await api.get(endpoint)
    expect(response.statusCode).toBe(200)
    expect(response.body.ok).toBe(true)
  })
  test('It responds with an array of all cities in database', async () => {
    const response = await api.get(endpoint)
    expect(response.body.total).toBe(2)
    expect(response.body).not.toBeNull()
    expect(Array.isArray(response.body.response)).toBe(true)
  })

  test('It responds with the correct data of cities', async () => {
    const response = await api.get(endpoint)

    expect(response.body.response[0]).toMatchObject(initialCities[0])
    expect(response.body.response[1]).toMatchObject(initialCities[1])
  })
  test('It responds with an empty array if the database if the collection is empty ', async () => {
    await City.deleteMany({})
    const response = await api.get(endpoint)
    expect(response.body.response.length).toBe(0)
  })
})

describe('GET /cities/:name', () => {
  const cityName = initialCities[0].name
  const incorrectCityName = 'notExist'
  const endpoint = '/api/cities/'

  test('It responds with status 200 OK', async () => {
    const response = await api.get(endpoint + cityName)
    expect(response.statusCode).toBe(200)
    expect(response.body.ok).toBe(true)
  })

  test('It responds with only one city', async () => {
    const response = await api.get(endpoint + cityName)
    expect(response.body).not.toBeNull()
    expect(response.body.total).toBe(1)
  })
  test('It responds with the correct city', async () => {
    const response = await api.get(endpoint + cityName)
    expect(response.body.response.name).toBe(cityName)
  })
  test('It responds with null because the city do not exists', async () => {
    const response = await api.get(endpoint + incorrectCityName)
    expect(response.body.response).toBeNull()
  })
})

// POST
describe('POST /cities', () => {
  const endpoint = '/api/cities'

  test('It responds with status 201', async () => {
    const newCity = {
      name: 'new name',
      img: 'new img',
      country: 'new country'
    }
    const response = await api.post(endpoint)
      .send(newCity)
    expect(response.statusCode).toBe(201)
    expect(response.body.response).toMatchObject(newCity)
  })

  test('It responds with the newly created city', async () => {
    const newCity = {
      name: 'new name',
      img: 'new img',
      country: 'new country'
    }
    const response = await api.post(endpoint)
      .send(newCity)
    expect(response.statusCode).toBe(201)
    expect(response.body.response).toMatchObject(newCity)
  })

  test('make sure a city was added to the database', async () => {
    const newCity = {
      name: 'new name',
      img: 'new img',
      country: 'new country'
    }
    await api.post(endpoint)
      .send(newCity)
    const response = await api.get('/api/cities')
    expect(response.body.response.length).toBe(initialCities.length + 1)
  })

  test('make sure a wrong object is not added', async () => {
    const newCity = {
      wrong: 'new name',
      img: 'new img',
      country: 'new country'
    }
    await api.post(endpoint)
      .send(newCity)
    const response = await api.get('/api/cities')
    expect(response.body.response.length).toBe(initialCities.length)
  })
})
