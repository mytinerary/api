require('dotenv').config()
const Server = require('../models/server')
const supertest = require('supertest')
const Itinerary = require('../models/itinerary.model')
const City = require('../models/city.model')

const server = new Server()
let testServer
let api
const initialCity = { name: 'Buenos Aires', img: 'buenosAires.jpg', country: 'Argentina' }

const initialItineraries = [
  {
    likes: 1,
    hashtags: [],
    userLike: [],
    title: 'Titulo',
    img: 'imagen',
    authorName: 'author',
    authorPic: 'pic',
    price: 2,
    duration: 3,
    cityId: '',
    activities: [],
    comments: []
  }
]
beforeAll(async () => {
  testServer = server.listen()
  api = supertest(testServer)
})
beforeEach(async () => {
  await Itinerary.deleteMany({})
  const city1 = new City(initialCity)
  const cityDb = await city1.save()
  console.log(cityDb)
  initialItineraries[0].cityId = cityDb._id
  const itinerary = new Itinerary(initialItineraries[0])
  await itinerary.save()
})

afterEach(async () => {
  await Itinerary.deleteMany({})
})

afterAll(async () => {
  await Itinerary.deleteMany({})

  await server.disconnectDB()
  testServer.close()
})

describe('GET /itineraries', () => {
  const endpoint = '/api/itineraries/all'
  test('It responds with status 200 OK', async () => {
    const response = await api.get(endpoint)
    expect(response.statusCode).toBe(200)
    expect(response.body.ok).toBe(true)
  })
  test('It responds with an array of all itineraries in database', async () => {
    const response = await api.get(endpoint)
    expect(response.body.total).toBe(initialItineraries.length)
    expect(response.body).not.toBeNull()
    expect(Array.isArray(response.body.itineraries)).toBe(true)
  })

  test('It responds with the correct data of itineraries', async () => {
    const response = await api.get(endpoint)

    expect(response.body.itineraries[0].title).toBe(initialItineraries[0].title)
  })
  test('It responds with an empty array if the collection is empty ', async () => {
    await Itinerary.deleteMany({})
    const response = await api.get(endpoint)
    expect(response.body.itineraries.length).toBe(0)
  })
})
