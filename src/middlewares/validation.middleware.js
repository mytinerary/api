const { validationResult } = require('express-validator')

const checksValidation = (req, res, next) => {
  const errors = validationResult(req)
  if (!errors.isEmpty()) {
    return res.status(400).json({
      success: false,
      message: 'Se encuentran errores en la request',
      error: errors.array({ onlyFirstError: true })
    })
  }
  next()
}
module.exports = checksValidation
