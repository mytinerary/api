const likeService = require('../services/like.service')

const likeItinerary = async (req, res) => {
  try {
    const { itineraryId } = req.params
    const { id: userId } = req.user
    const result = await likeService.likeItinerary(userId, itineraryId)
    res.status(200).json({
      success: true,
      response: result
    })
  } catch (error) {
    res.status(500).send({
      ok: false,
      message: 'Error Interno del Servidor',
      err: error.message
    })
  }
}

module.exports = {
  likeItinerary
}
