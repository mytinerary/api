const itineraryService = require('../services/itinerary.service')

const getAll = async (req, res) => {
  try {
    const result = await itineraryService.getAll()
    res.status(200).json(result)
  } catch (error) {
    res.status(500).send({
      ok: false,
      message: 'Error Interno del Servidor',
      err: error
    })
  }
}
const getByCityName = async (req, res) => {
  try {
    const result = await itineraryService.getByCityName(req.params.cityName)
    res.status(200).json(result)
  } catch (error) {
    res.status(500).send({
      ok: false,
      message: 'Error Interno del Servidor',
      err: error.message
    })
  }
}
const getByCityId = async (req, res) => {
  try {
    const result = await itineraryService.getByCityId(req.params.cityId)
    res.status(200).json(result)
  } catch (error) {
    res.status(500).send({
      ok: false,
      message: 'Error Interno del Servidor',
      err: error.message
    })
  }
}

const create = async (req, res) => {
  try {
    const newItinerary = {
      title: req.body.title,
      img: req.body.img,
      authorName: req.user.firstName,
      authorPic: req.user.userPic,
      price: req.body.price,
      duration: req.body.duration,
      hashtags: req.body.hashtags,
      cityId: req.body.cityId,
      activities: req.body.activities,
      comments: [],
      userLike: []
    }
    const result = await itineraryService.create(newItinerary)
    res.status(201).json(result)
  } catch (error) {
    res.status(500).json({
      ok: false,
      message: 'Error interno del servidor',
      error: error.message
    })
  }
}

module.exports = {
  getAll,
  getByCityName,
  getByCityId,
  create
}
