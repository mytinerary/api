const itineraryService = require('../services/itinerary.service')

const getCommentsByItineraryId = async (req, res) => {
  try {
    const { itineraryId } = req.params
    const { id: userId } = req.user
    const result = await itineraryService.getCommentsByItineraryId(itineraryId, userId)
    res.status(201).json({
      success: true,
      response: {
        comments: result.comments,
        arrayUserComments: result.arrayUserComments
      }
    })
  } catch (error) {
    res.status(500).send({
      success: false,
      message: 'Error Interno del Servidor',
      err: error.message
    })
  }
}

const commentItinerary = async (req, res) => {
  const { itineraryId } = req.params
  const user = req.user
  const { text } = req.body
  try {
    const result = await itineraryService.commentItinerary(itineraryId, user, text)
    res.status(200).json({
      success: true,
      response: result.comments,
      arrayOwnerCheck: result.arrayOwnerCheck

    })
  } catch (error) {
    res.status(500).json({
      success: false,
      message: 'Error interno del servidor',
      error: error.message
    })
  }
}

const editItineraryComment = async (req, res) => {
  const { commentId } = req.params
  const { id: userId } = req.user
  const { text } = req.body
  try {
    const result = await itineraryService.editComment(commentId, userId, text)
    res.status(200).json({
      success: true,
      response: result.comments
    })
  } catch (error) {
    res.status(500).json({
      success: false,
      message: 'Error interno del servidor',
      error: error.message
    })
  }
}
const deleteItineraryComment = async (req, res) => {
  const { commentId } = req.params
  const { id: userId } = req.user
  try {
    const result = await itineraryService.deleteComment(commentId, userId)
    res.status(200).json({
      success: true,
      response: result.comments
    })
  } catch (error) {
    res.status(500).json({
      success: false,
      message: 'Error interno del servidor',
      error: error.message
    })
  }
}
module.exports = {
  getCommentsByItineraryId,
  commentItinerary,
  editItineraryComment,
  deleteItineraryComment
}
