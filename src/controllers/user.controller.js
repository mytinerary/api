const userService = require('../services/user.service')

const getAll = async (req, res) => {
  try {
    const result = await userService.getAll()

    res.status(200).json(result)
  } catch (error) {
    res.status(500).send({
      success: false,
      message: 'Error Interno del Servidor',
      err: error.message
    })
  }
}

const getById = async (req, res) => {
  try {
    const { id } = req.params
    const result = await userService.getById(id)
    res.status(200).json(result)
  } catch (error) {
    res.status(500).send({
      success: false,
      message: 'Error Interno del Servidor',
      err: error.message
    })
  }
}

const create = async (req, res) => {
  try {
    const newUser = {
      firstName: req.body.firstName,
      lastName: req.body.lastName,
      email: req.body.email,
      password: req.body.password,
      userPic: req.body.userPic,
      country: req.body.country
    }
    const result = await userService.create(newUser)
    res.status(201).json({
      success: true,
      response: result
    })
  } catch (error) {
    res.status(500).json({
      success: false,
      message: 'Error interno del servidor',
      error: error.message
    })
  }
}

const signin = async (req, res) => {
  try {
    const user = {
      email: req.body.email,
      password: req.body.password
    }
    const result = await userService.signin(user)
    res.status(200).json({
      success: true,
      response: result
    })
  } catch (error) {
    res.status(500).json({
      success: false,
      message: 'Error interno del servidor',
      error: error.message
    })
  }
}
const signinls = async (req, res) => {
  try {
    const user = {
      userPic: req.user.userPic,
      firstName: req.user.firstName
    }
    res.status(200).json({
      success: true,
      response: user
    })
  } catch (error) {
    res.status(500).json({
      success: false,
      message: 'Error interno del servidor',
      error: error.message
    })
  }
}

module.exports = {
  getAll,
  getById,
  create,
  signin,
  signinls
}
