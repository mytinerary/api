const itineraryService = require('../services/itinerary.service')
const userService = require('../services/user.service')

const getCheckuser = async (req, res) => {
  try {
    const { itineraryId } = req.params
    const { id: userId } = req.user
    const result = await itineraryService.getCommentsByItineraryId(itineraryId, userId)
    const isLiked = await userService.isItineraryLikedByUserId(userId, itineraryId)

    res.status(200).json({
      success: true,
      response: {
        arrayOwnerCheck: result.arrayUserComments.map(userComment => userComment._id),
        likedCheck: isLiked
      }
    })
  } catch (error) {
    res.status(500).send({
      success: false,
      message: 'Error Interno del Servidor',
      err: error.message
    })
  }
}

module.exports = {
  getCheckuser
}
