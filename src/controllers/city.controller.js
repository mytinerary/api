const cityService = require('../services/city.service')

const getAll = async (req, res) => {
  try {
    const queryParams = req.query
    let result
    if (Object.keys(queryParams).length !== 0) {
      result = await cityService.getByQuery(queryParams)
    } else {
      result = await cityService.getAll()
    }

    res.status(200).json(result)
  } catch (error) {
    res.status(500).send({
      ok: false,
      message: 'Error Interno del Servidor',
      err: error.message
    })
  }
}
const getByName = async (req, res) => {
  try {
    const { name } = req.params
    const result = await cityService.getByName(name)
    res.status(200).json(result)
  } catch (error) {
    res.status(500).send({
      ok: false,
      message: 'Error Interno del Servidor',
      err: error.message
    })
  }
}
const getByQuery = async (req, res) => {
  const queryParams = req.query
  try {
    const result = await cityService.getByQuery(queryParams)
    res.status(200).json(result)
  } catch (error) {
    res.status(500).send({
      ok: false,
      message: 'Error Interno del Servidor',
      err: error.message
    })
  }
}
const create = async (req, res) => {
  try {
    const newCity = {
      name: req.body.name,
      country: req.body.country,
      img: req.body.img
    }
    const result = await cityService.create(newCity)
    res.status(201).json(result)
  } catch (error) {
    res.status(500).json({
      ok: false,
      message: 'Error interno del servidor',
      error: error.message
    })
  }
}

module.exports = {
  getAll,
  create,
  getByQuery,
  getByName
}
