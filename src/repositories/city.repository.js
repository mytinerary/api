const City = require('../models/city.model')

const getAll = async () => await City.find()
const getById = async (id) => await City.findById(id)

const getByQuery = async (queryParams) => await City.find(queryParams)

const create = async (city) => {
  const newCity = new City({
    name: city.name,
    country: city.country,
    img: city.img
  })
  return await newCity.save()
}
const getByName = async (name) => {
  return await City.findOne({ name })
}

module.exports = {
  getAll,
  create,
  getByName,
  getByQuery,
  getById
}
