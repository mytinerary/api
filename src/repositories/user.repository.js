const User = require('../models/user.model')

const getAll = async () => await User.find()
const getById = async (id) => await User.findOne({ _id: id })
const getByEmail = async (email) => await User.findOne({ email })

const create = async (user) => {
  const newUser = new User({
    firstName: user.firstName,
    lastName: user.lastName,
    email: user.email,
    password: user.password,
    userPic: user.userPic,
    country: user.country,
    favoriteItineraries: []
  })
  return await newUser.save()
}
const getLikesByUserId = async (userId) => await User.findOne({ _id: userId }, 'favoriteItineraries')

const update = async (filter, update) => await User.updateOne(filter, update)

module.exports = {
  getAll,
  getById,
  getByEmail,
  create,
  update,
  getLikesByUserId
}
