const Itinerary = require('../models/itinerary.model')

const getAll = async () => await Itinerary.find().populate('cityId')
const getById = async (id) => await Itinerary.findOne({ _id: id })
const create = async (itinerary) => {
  const newItinerary = new Itinerary(itinerary)
  return await newItinerary.save()
}
const getByCityId = async (cityId) => await Itinerary.find({ cityId: cityId }).populate('cityId')
const update = async (filter, update) => await Itinerary.updateOne(filter, update)
const getItineraryByQuery = async (filter) => await Itinerary.findOne(filter)
const findAndUpdateOneItinerary = async (filter, update, option) => await Itinerary.findOneAndUpdate(filter, update, option)
module.exports = {
  getAll,
  getByCityId,
  getById,
  update,
  getItineraryByQuery,
  findAndUpdateOneItinerary,
  create
}
