const express = require('express')
const { check } = require('express-validator')

const router = express.Router()
const entityController = require('../controllers/city.controller')
const checksValidation = require('../middlewares/validation.middleware')

router.get('/', entityController.getAll)
router.get('/:name', entityController.getByName)

router.post('/', [
  check('name', 'Must add a valid name').isString().not().isEmpty(),
  check('country', 'Must add a valid country').isString().not().isEmpty(),
  check('img', 'Must add a valid img').isString().not().isEmpty(),
  checksValidation
], entityController.create)

module.exports = router
