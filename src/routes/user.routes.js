const express = require('express')
const { check } = require('express-validator')
const passport = require('../../passport')
const router = express.Router()
const entityController = require('../controllers/user.controller')
const checksValidation = require('../middlewares/validation.middleware')

router.get('/signinls',
  passport.authenticate('jwt', { session: false }),
  entityController.signinls
)
router.post('/signin/', entityController.signin)

router.get('/:id', [
  check('email', 'Must add a valid email').isEmail(),
  check('password', 'Must add a valid password').notEmpty(),
  checksValidation
], entityController.getById)

router.get('/',
  passport.authenticate('jwt', { session: false }),
  entityController.getAll)

router.post('/signup', [
  check('firstName', 'Must add a valid firstName').isString().notEmpty(),
  check('lastName', 'Must add a valid lastName').isString().notEmpty(),
  check('email', 'Must add a valid email').isEmail(),
  check('password', 'Must add a valid password').notEmpty(),
  check('userPic', 'Must add a valid userPic').isString().notEmpty(),
  check('country', 'Must add a valid country').isString().notEmpty(),
  checksValidation
], entityController.create)

module.exports = router
