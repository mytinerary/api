const express = require('express')
const passport = require('../../passport')
const router = express.Router()
const entityController = require('../controllers/checkuser.controller')

router.get('/:itineraryId',
  passport.authenticate('jwt', { session: false }),
  entityController.getCheckuser)

module.exports = router
