const express = require('express')
const { check } = require('express-validator')
const passport = require('../../passport')
const router = express.Router()
const entityController = require('../controllers/comment.controller')
const checksValidation = require('../middlewares/validation.middleware')

router.post('/:itineraryId',
  passport.authenticate('jwt', { session: false }),
  check('text', 'Must add a text').isString().notEmpty(),
  check('text', 'Must be a string').isString(),
  checksValidation,
  entityController.commentItinerary)

router.put('/:commentId',
  passport.authenticate('jwt', { session: false }),
  check('text', 'Must add a text').isString(),
  check('text', 'Must be a string').isString(),

  checksValidation,
  entityController.editItineraryComment)

router.delete('/:commentId',
  passport.authenticate('jwt', { session: false }),
  entityController.deleteItineraryComment)

router.get('/:itineraryId',
  passport.authenticate('jwt', { session: false }),
  entityController.getCommentsByItineraryId)

module.exports = router
