const express = require('express')
const passport = require('../../passport')
const router = express.Router()
const entityController = require('../controllers/like.controller')

router.get('/:itineraryId',
  passport.authenticate('jwt', { session: false }),
  entityController.likeItinerary)

module.exports = router
