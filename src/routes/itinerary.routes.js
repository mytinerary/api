const express = require('express')
const router = express.Router()
const { check } = require('express-validator')
const passport = require('../../passport')

const entityController = require('../controllers/itinerary.controller')
const checksValidation = require('../middlewares/validation.middleware')

router.get('/all', entityController.getAll)

router.get('/:cityId', entityController.getByCityId)

router.post('/',
  passport.authenticate('jwt', { session: false }),
  check('cityId', 'Must add a valid cityId').isString().notEmpty(),
  check('title', 'Must add a valid title').isString().notEmpty(),
  check('img', 'Must add a valid img').isString().notEmpty(),
  check('authorName', 'Must add a valid authorName').notEmpty(),
  check('authorPic', 'Must add a valid authorPic').isString().notEmpty(),
  check('price', 'Must add a valid price').isInt().notEmpty(),
  check('duration', 'Must add a valid duration').isInt().notEmpty(),
  checksValidation
  , entityController.create)
module.exports = router
