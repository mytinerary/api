const userRepository = require('../repositories/user.repository')
const bcrypt = require('bcrypt')
const jwt = require('jsonwebtoken')
const secretKey = process.env.SECRET_KEY
const saltRounds = 10

const getAll = async () => {
  const usersDb = await userRepository.getAll()
  const count = usersDb.length
  return {
    success: true,
    total: count,
    response: usersDb
  }
}

const getById = async (id) => {
  const userDb = await userRepository.getById(id)
  const count = userDb ? 1 : 0

  return {
    success: true,
    total: count,
    response: userDb
  }
}

const create = async (user) => {
  const duplicated = await getByEmail(user.email)
  if (duplicated) {
    throw new Error('El email ingresado ya se encuentra registrado')
  }
  user.password = bcrypt.hashSync(user.password, saltRounds)
  const userDb = await userRepository.create(user)
  const payload = {
    user: {
      id: userDb.id,
      firstName: userDb.firstName,
      userPic: userDb.userPic
    }
  }
  const options = { expiresIn: 2592000 }
  const token = jwt.sign(
    payload,
    secretKey,
    options
  )
  return {
    success: true,
    message: 'El usuario se creó correctamente',
    response: {
      token: token,
      userPic: userDb.userPic,
      firstName: userDb.firstName
    }
  }
}

const getByEmail = async (email) => await userRepository.getByEmail(email)

const signin = async (user) => {
  const userDb = await getByEmail(user.email)
  if (!userDb) throw new Error('El email no se corresponde con un usuario registrado')
  const verifyPassword = bcrypt.compareSync(user.password, userDb.password)
  if (!verifyPassword) throw new Error('La contraseña es incorrecta')
  const payload = {
    user: {
      id: userDb.id,
      firstName: userDb.firstName,
      userPic: userDb.userPic
    }
  }
  const options = { expiresIn: 2592000 }
  const token = jwt.sign(
    payload,
    secretKey,
    options
  )
  return {
    token: token,
    userPic: userDb.userPic,
    firstName: userDb.firstName
  }
}
const isItineraryLikedByUserId = async (userId, itineraryId) => {
  const { favoriteItineraries } = await userRepository.getLikesByUserId(userId)
  if (favoriteItineraries?.includes(itineraryId)) return true
  return false
}

module.exports = {
  getAll,
  getById,
  getByEmail,
  create,
  signin,
  isItineraryLikedByUserId
}
