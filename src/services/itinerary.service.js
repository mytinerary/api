const itineraryRepository = require('../repositories/itinerary.repository')
const { getById: getCityById } = require('./city.service')

const getAll = async () => {
  const itinerariesDb = await itineraryRepository.getAll()
  const count = itinerariesDb.length
  if (!itinerariesDb) {
    return {
      ok: false,
      message: ''
    }
  }

  return {
    ok: true,
    message: 'Itinerarios',
    itineraries: itinerariesDb,
    total: count
  }
}

const getByCityId = async (cityId) => {
  const itinerariesDb = await itineraryRepository.getByCityId(cityId)
  const count = itinerariesDb.length
  if (count === 0) {
    return {
      ok: false,
      message: 'No se encontraron registros'
    }
  }
  return {
    success: true,
    message: 'Itinerarios',
    response: itinerariesDb,
    total: count
  }
}

const getCommentsByItineraryId = async (itineraryId, userId) => {
  const itineraryDb = await itineraryRepository.getById(itineraryId)
  const { comments } = itineraryDb

  const userCommentsId = comments?.filter(comment => {
    return comment.userId.toString() === userId.toString()
  })
  return {
    comments,
    arrayUserComments: userCommentsId
  }
}

const commentItinerary = async (itineraryId, user, text) => {
  const updatedItinerary = await itineraryRepository.findAndUpdateOneItinerary(
    { _id: itineraryId },
    { $push: { comments: { text, userId: user._id, userName: user.firstName, userPic: user.userPic } } },
    { new: true })

  const { arrayUserComments } = await getCommentsByItineraryId(updatedItinerary._id, user._id)
  const arrayOwnerCheck = arrayUserComments.map(userComment => userComment._id)

  return {
    comments: updatedItinerary?.comments,
    arrayOwnerCheck
  }
}

const editComment = async (commentId, userId, text) => {
  const itineraryDB = await itineraryRepository.getItineraryByQuery({ 'comments._id': commentId, 'comments.userId': userId })

  if (!itineraryDB) {
    throw new Error('Can not edit this comment')
  }

  const updatedItinerary = await itineraryRepository.findAndUpdateOneItinerary(
    { 'comments._id': commentId },
    { $set: { 'comments.$.text': text } },
    { new: true })

  return {
    comments: updatedItinerary.comments
  }
}

const deleteComment = async (commentId, userId) => {
  const itineraryDB = await itineraryRepository.getItineraryByQuery({ 'comments._id': commentId, 'comments.userId': userId })

  if (!itineraryDB) throw new Error('Can not edit this comment')

  const updatedItinerary = await itineraryRepository.findAndUpdateOneItinerary(
    { 'comments._id': commentId },
    { $pull: { comments: { _id: commentId } } },
    { new: true })

  return {
    comments: updatedItinerary.comments
  }
}

const create = async (newItinerary) => {
  const cityDb = getCityById(newItinerary.cityId)
  if (!cityDb) throw new Error('the specified city does not exist')
  const itineraryDb = await itineraryRepository.create(newItinerary)
  return {
    ok: true,
    message: 'La Ciudad se creo correctamente',
    response: itineraryDb
  }
}

module.exports = {
  getAll,
  commentItinerary,
  getCommentsByItineraryId,
  getByCityId,
  editComment,
  deleteComment,
  create
}
