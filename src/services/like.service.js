const itineraryRepository = require('../repositories/itinerary.repository')

const likeItinerary = async (userId, itineraryId) => {
  const itineraryToLike = await itineraryRepository.getItineraryByQuery({ _id: itineraryId, userLike: userId })

  const filter = itineraryToLike ? '$pull' : '$push'
  const liked = !itineraryToLike

  const itineraryLiked = await itineraryRepository.findAndUpdateOneItinerary({ _id: itineraryId }, { [filter]: { userLike: userId } }, { new: true })

  let likesFromDb = parseInt(itineraryLiked.likes)

  const itineraryModified = await itineraryRepository.findAndUpdateOneItinerary({ _id: itineraryId }, { $set: { likes: likesFromDb += liked ? 1 : -1 } }, { new: true })

  return {
    likes: itineraryModified?.likes, liked
  }
}

module.exports = {
  likeItinerary
}
