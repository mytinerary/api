const cityRepository = require('../repositories/city.repository')

const getById = async () => {
  const cityDb = await cityRepository.getById()
  return {
    ok: true,
    response: cityDb
  }
}
const getAll = async () => {
  const citiesDb = await cityRepository.getAll()
  const count = citiesDb.length
  return {
    ok: true,
    total: count,
    response: citiesDb
  }
}
const getByName = async (name) => {
  const cityDb = await cityRepository.getByName(name)
  const count = cityDb ? 1 : 0

  return {
    ok: true,
    total: count,
    response: cityDb
  }
}
const getByQuery = async (queryParams) => {
  const citiesDb = await cityRepository.getByQuery(queryParams)
  const count = citiesDb.length

  return {
    ok: true,
    total: count,
    response: citiesDb
  }
}

const create = async (city) => {
  const duplicated = await cityRepository.getByName(city.name)
  if (duplicated) {
    throw new Error('El nombre de la ciudad ya existe')
  }

  const cityDb = await cityRepository.create(city)
  return {
    ok: true,
    message: 'La Ciudad se creo correctamente',
    response: cityDb
  }
}

module.exports = {
  getAll,
  create,
  getByQuery,
  getByName,
  getById
}
