const mongoose = require('mongoose')
const { MONGO_URI, MONGO_URI_TEST, NODE_ENV } = process.env
const mongoURI = NODE_ENV === 'test' ? MONGO_URI_TEST : MONGO_URI

const connectDB = () => mongoose.connect(mongoURI, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  useFindAndModify: false
}).then(res => console.log('Successfully connected to MongoDB'))
  .catch(err => console.log(`A connection error occurred: ${err}`))

const closeConnection = () => mongoose.connection.close().then(res => console.log('Successfully disconnected to MongoDB'))
  .catch(err => console.log(`An error occurred: ${err}`))

module.exports = { connectDB, closeConnection }
