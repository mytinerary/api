  

# MyTinerary API ✈️

  

## Como Utilizar 🤔

⚠️ Descargar cliente desde
https://gitlab.com/mytinerary/client/

### Crear un archivo .env

    PORT= {puerto de escucha de la aplicación}
    
    MONGO_URI= {string de conexion a la bd}
    
    MONGO_URI_TEST= {string de conexion a la bd de test}
    
    SECRET_KEY= {clave para jwt}

### Instalar dependencias
Tanto en la api como en el cliente:

    npm install 🔨



  

### Iniciar proyecto
En Api:

    npm start 🚀

  
<hr>

# Detalles de los distintos sprint
  

## Sprint 1️⃣

  

- Instalar Postman

  

- Crear de base de datos en **MongoDB Atlas**

  

- Crear de coleccion **cities**

  

- Crear proyecto en Node

  

- Instalar Express, Mongoose y Nodemon

  

- Inicializar servidor

  

- Vincular base de datos

  

- Crear modelo cities con **mongoose**

  

- Ruta **GET/cities** Devuelve todos las ciudades

  

- Ruta **POST/cities** Inserta una ciudad en la base de datos

  

  

## Refactorización de codigo

  

-Desacoplar codigo en rutas, controladores, servicios y repositorios

  

  

## Implementación de TDD

Implementar Jest y supertest para testear endpoints

  

  

## Adaptacion

  

Realizar los cambios necesarios para brindar la informacion necesaria al frontend (módulo 1)

  

## Sprint 2️⃣

  

### Creacion de itinerario

  

- Definición de modelo

  

- Ruta **GET/itineraries:** Devuelve todos los itinerarios

  

- Ruta **GET/itineraries/:cityName** Devuelve todos los itinerarios por ciudad

  

## Sprint 3️⃣

  

### Registro de usuario

  

- Validar que no exista un usuario con el mismo email

  

- Contraseña cifrada a traves de **Bcrypt**

  

### Validación de request a traves de **express-validation**

  

### Autenticación

  

Utilizar Passport y JWT

  

## Sprint 4️⃣

### Itinerarios

- Comentarios: Agregar, editar y eliminar

- Likes: Dar y quitar

## Login con localStorage

- Sesion persistente a traves de Jwt y passport
